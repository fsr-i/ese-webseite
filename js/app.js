
document.addEventListener('DOMContentLoaded', ()=>{
	let app = new Vue({
		el: '#app',
		data: {
			showSidenav: false
		},
		methods: {
			toggleSidenav: function(state) {
				if (state === undefined) {
					this.showSidenav = !this.showSidenav;
				} else {
					this.showSidenav = state
				}
				if (this.showSidenav) {
					showTopnav();
				}
			},
			jump: function(target) {
				if (typeof target === 'string') {
					target = document.querySelector(target);
				}
				this.$nextTick(()=>{target.scrollIntoView();});
			}
		}
	});

	let barState = 'fixed'; // 'scrollover'; // 'hidden';
	let bar = document.querySelector('nav header.topnav');

	function startedScroll(e) {
		if ((barState == 'fixed' && e.delta > 0) || (barState == 'hidden' && e.delta < 0)) {
			//console.log('scrollover');
			barState = 'scrollover';
			bar.style.position = 'absolute';
			if (e.delta > 0) {
				bar.style.top = e.pageY - e.delta + 'px';
			} else {
				bar.style.top = e.pageY - bar.offsetHeight - e.delta + 'px'
			}
		}
	}
	function intermediateScroll(e) {
		if (barState == 'scrollover') {
			let delta = e.pageY - bar.offsetTop;

			if (delta <= 0) {
				//console.log('fixed');
				barState = 'fixed';
				bar.style.position = 'fixed';
				bar.style.top = '0px';
			} else if (delta > bar.offsetHeight) {
				//console.log('hidden');
				barState = 'hidden';
				bar.style.position = 'fixed';
				bar.style.top = -bar.offsetHeight + 'px';
			}
		}
	}
	function stopScroll(e) {
		if (barState == 'scrollover') {
			showTopnav();
		}
	}
	function showTopnav() {
		bar.style.top = window.pageYOffset + 'px';
		bar.style.position = 'absolute';
	}

	let sd = new ScrollDetector();
	sd.addEventListener('start', startedScroll);
	sd.addEventListener('intermediate', intermediateScroll);
	sd.addEventListener('stop', stopScroll);
});

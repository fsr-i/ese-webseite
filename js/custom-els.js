
Vue.component('fsr', {
	template: '<span><span class="FSR" title="Fachschaftsrat">FSR</span><slot></slot></span>'
});
Vue.component('fsr-long', {
	template: '<span><span class="fsr">Fachschaftsrat</span><slot></slot></span>'
});
Vue.component('fsri', {
	template: '<span><span class="FSR" title="Fachschaftsrat Informatik">FSR&nbsp;<i>I</i></span><slot></slot></span>'
});
Vue.component('fsri-long', {
	template: '<span><span class="fsr">Fachschaftsrat Informatik</span><slot></slot></span>'
});


Vue.component('fsra-long', {
	template: '<span><span class="fsr">Fachschaftsräten</span><slot></slot></span>'
});


Vue.component('logo-x', {
	template: '<div class="fsr-logo"></div>'
});
Array.from('abcdef').forEach(e => {
	Vue.component('logo-'+e, {
		template: '<div class="fsr-logo '+e+'"></div>'
	});
	Vue.component('logo-'+e+'-r', {
		template: '<div class="fsr-logo '+e+' reduced"></div>'
	});
});

Vue.component('svg-hamburger', {
	props: ['color'],
	template: '<svg width="32px" height="32px"><path fill="{{ color }}" d="m5 10h22c1.104 0 2-0.896 2-2s-0.896-2-2-2h-22c-1.104 0-2 0.896-2 2s0.896 2 2 2zm22 4h-22c-1.104 0-2 0.896-2 2s0.896 2 2 2h22c1.104 0 2-0.896 2-2s-0.896-2-2-2zm0 8h-22c-1.104 0-2 0.896-2 2s0.896 2 2 2h22c1.104 0 2-0.896 2-2s-0.896-2-2-2z"/></svg>'
});

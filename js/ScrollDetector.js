
class ScrollDetector {
	constructor(elem, sensitivity, lStart, lStop, lInt) {
		this._elem = elem || window;
		this.sensitivity = sensitivity || 100;
		this._lStart = lStart ? (lStart instanceof Array ? lStart : [lStart]) : [];
		this._lStop = lStop ? (lStop instanceof Array ? lStop : [lStop]) : [];
		this._lInt = lInt ? (lInt instanceof Array ? lInt : [lInt]) : [];

		this._scrollTimer = null;
		this._lastPos = window.pageYOffset;
		this._lastDir = false;
		this._elem.addEventListener('scroll', this._digestScroll.bind(this));
	}
	
	get elem() {
		return this._elem;
	}
	set elem(el) {
		this._elem.removeEventListener('scroll', this._digestScroll);
		this._elem = el
		this._elem.addEventListener('scroll', this._digestScroll.bind(this));
	}

	addEventListener(event, cb) {
		//console.log('registering for '+event+':',cb);
		if (event.toLowerCase() == 'start') {
			if (!(cb instanceof Array)) {
				cb = [cb];
			}
			this._lStart.push.apply(this._lStart, cb);
		} else if (event.toLowerCase() == 'intermediate') {
			if (!(cb instanceof Array)) {
				cb = [cb];
			}
			this._lInt.push.apply(this._lInt, cb);
		} else if (event.toLowerCase() == 'stop') {
			if (!(cb instanceof Array)) {
				cb = [cb];
			}
			this._lStop.push.apply(this._lStop, cb);
		} else {
			return false;
		}
	}
	removeEventListener(event, cb) {
		if (event.toLowerCase() == 'start') {
			if (cb in this._lStart) {
				let i = this._lStart.indexOf(cb)
				this._lStart.splice(i,1);
			}
		} else if (event.toLowerCase() == 'stop') {
			if (cb in this._lStop) {
				let i = this._lStop.indexOf(cb)
				this._lStop.splice(i,1);
			}
		} else if (event.toLowerCase() == 'intermediate') {
			if (cb in this._lInt) {
				let i = this._lInt.indexOf(cb)
				this._lInt.splice(i,1);
			}
		} else {
			return false;
		}
	}

	_digestScroll(e) {
		if (!('pageY' in e)) {
			e.pageX = window.pageXOffset;
			e.pageY = window.pageYOffset;
		}
		e.delta = e.pageY - this._lastPos;
		e.dir = e.delta > 0;

		if (this._scrollTimer === null) {
			this._handleStart(e);
		} else {
			if (this._lastDir != e.dir) {
				this._handleStop(e);
				this._handleStart(e);
			} else {
				this._handleIntermediate(e);
			}

			clearTimeout(this._scrollTimer);
		}

		this._scrollTimer = setTimeout((e=>{
			this._scrollTimer = null;
			this._handleStop(e);
		}).bind(this, e), this.sensitivity);

		this._lastPos = e.pageY;
		this._lastDir = e.dir;
	}

	_handleStart(e) {
		//console.log('_handleStart', this._lStart);
		this._lStart.forEach(cb => {
			cb(e);
		});
	}
	_handleIntermediate(e) {
		//console.log('_handleIntermediate', this._lInt);
		this._lInt.forEach(cb => {
			cb(e);
		});
	}
	_handleStop(e) {
		//console.log('_handleStop', this._lStop);
		this._lStop.forEach(cb => {
			cb(e);
		});
	}
}

<?php
$file = ".currentyear";
$year = intval(file_get_contents($file));

if (isset($_GET["archive"])) {
	$current_year = $year;
	$year = intval($_GET["archive"]);
} else {
	if ($year < 2000 || $year > 2999) {
		$year = intval(date("Y"));
	}
}
$shortyear = $year % 100;

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ESE <?php echo $shortyear; echo isset($current_year) ? " (archive)" : ""; ?> — FSR–I an der HSZG</title>
	<link rel="stylesheet" type="text/css" href="css/base.css">

	<!-- development version, includes helpful console warnings -->
	<!--<script src="https://cdn.jsdelivr.net/npm/vue@2.7.8/dist/vue.js"></script>-->
	<!-- production version, optimized for size and speed -->
	<script src="https://cdn.jsdelivr.net/npm/vue@2.7.8"></script>

	<script src="js/app.js"></script>
	<script src="js/custom-els.js"></script>
	<script src="js/ScrollDetector.js"></script>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="fa/css/all.css">
</head>
<body>
<div id="app">

<nav v-bind:class="{ 'show-sidenav': showSidenav }">
	<header class="topnav">
		<ul>
			<li class="hamburger-menu stay-ontop"><a href="#" v-on:click.prevent="toggleSidenav()"><svg-hamburger/><!--&#9776;--></a></li>
			<li class="logo stay-ontop"><a href="https://fsr-i.de"><logo-b-r/></a></li>
			<!--<li class="recent right"><a href="#">R</a></li>-->
<?php
if (isset($current_year)) {
?>
			<li>
				<h2 style="font-size: max(0.6em, min(1.6em, 2.8vw)); margin: 0; word-break: break-word; height: 100%; text-transform: initial; 
				max-width: calc(100vw - 170px);">
					<a href="?" style="text-decoration: underline;">Viewing the archive. Go to the current version of <?php echo $current_year; ?>.</a>
				</h2>
			</li>
<?php } ?>
		</ul>
	</header>

	<div class="sidenav-fade" v-on:click="toggleSidenav(false)"></div>
	<div class="sidenav dark-theme" style="z-index: 99;">
		<div class="top"></div>
		<ul>
			<li><a href="#ese" v-on:click="toggleSidenav(false)">ESE <?php echo $shortyear; ?></a></li>
			<li><a href="#ueber-den-fsri" v-on:click="toggleSidenav(false)">Über den <FSRI/></a></li>
			<li><a href="mailto:fsi@hszg.de" v-on:click="toggleSidenav(false)">Mail</a></li>
			<li><a href="https://www.instagram.com/fsr_informatik/" v-on:click="toggleSidenav(false)">Instagram</a></li>
			<li><a href="https://matrix.to/#/#fsri-ankuendigungen:hszg.de" v-on:click="toggleSidenav(false)">Matrix</a></li>
			<li><a href="https://discord.fsr-i.de" v-on:click="toggleSidenav(false)">Discord</a></li>
		</ul>
	</div>
</nav>

<div class="app-content">

	<section id="ese" style="margin-bottom: 5em;">
		<div class="banner c-ese">
			<h1>Willkommen zur <b>ESE&nbsp;<?php echo $shortyear; ?></b>!</h1>
			<p>Dein Start ins Informatikstudium</p>
		</div>
		<div>
			<p>
				Um dir deinen Studienstart zu erleichtern, veranstalten wir die <b>E</b>rst<b>S</b>emester<b>E</b>inführung (ESE). Hier kannst du uns 
				und deine Kommilitonen schon mal etwas kennenlernen und bekommst ein paar Infos.
			</p>

			<div class="highlight">
				<p><i class="far fa-calendar-alt"></i> <a href="<?php echo $year; ?>/zeitplan-screen.pdf"><button>Hier</button></a> kannst du dir hier 
				den aktuellen Zeitplan runterladen.</p>
				<p><i class="fas fa-book-open"></i> Wenn du deinen Erstiführer gerade nicht zur Hand hast, kannst du ihn 
				<a href="<?php echo $year; ?>/manual-screen_compressed.pdf"><button>hier digital ansehen</button></a>.</p>
			</div>

			<p>Außerdem kannst du dich mit allen großen und kleinen Fragen jederzeit an uns wenden: <a href="mailto:fsi@hszg.de">fsi@hszg.de</a></p>
		</div>
	</section>
	<section id="ueber-den-fsri">
		<div class="banner c-ese">
			<h1>Über den <fsri/><!--<logo-x/>--></h1>
			<!--<p>Aufgaben, Ziele, Mitglieder, Projekte</p>-->
		</div>
		<div>
			<p>
				Der <fsr-long/> ist die Interessenvertretung einer Fachschaft (z.B. in höheren <a href="//fsr-i.de/#gremien">Gremien</a> wie dem 
				<a href="https://f-ei.hszg.de/fakultaet/gremien/fakultaetsrat">Fakultätsrat</a>). Er ist Ansprechpartner für alle Fragen des Studiums 
				sowie des Studentenlebens und Hauptvermittler zwischen den Studierenden und den Dozierenden/Professoren. Er organisiert Veranstaltungen 
				und Projekte, auch in Kooperation mit anderen <fsra-long/>.
			</p>
			<p>
				Wie jedes Gremium hält auch der <FSR/> regelmäßige hochschulöffentliche 
				<a href="https://f-ei.hszg.de/informationen-fuer-studierende/fachschaftsrat-informatik/sitzungstermine">Sitzungen</a>, in denen 
				<a href="https://f-ei.hszg.de/informationen-fuer-studierende/fachschaftsrat-informatik/protokolle">Protokoll</a> 
				geführt wird. Der <FSR/> ist verpflichtet, diese jedem Mitglied der Fachschaft und dem <a href="https://stura.hszg.de/">StuRa</a> 
				auf Verlangen vorzuzeigen.
			</p>

			<div class="highlight">
				<p>
					<a href="https://www.instagram.com/fsr_informatik/"><i class="icon instagram"></i> <FSRI/>-Instagram: <a href="https://www.instagram.com/fsr_informatik/">https://www.instagram.com/fsr_informatik/</a>
				</p>
				<p>
					<a href="https://matrix.to/#/#fsri-ankuendigungen:hszg.de"><i class="icon matrix"></i> <FSRI/>-Matrix News-Channel: <a href="https://matrix.to/#/#fsri-ankuendigungen:hszg.de">https://matrix.to/#/#fsri-ankuendigungen:hszg.de</a>
				</p>
				<p>
					<a href="https://discord.com/invite/Js29ZMxBgt"><i class="icon discord"></i> <FSRI/>-Discord: <a href="https://discord.com/invite/Js29ZMxBgt">https://discord.com/invite/Js29ZMxBgt</a>
				</p>
			</div>
		</div>
	</section>

</div>

</div>


	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,800" rel="stylesheet">
</body>
</html>
